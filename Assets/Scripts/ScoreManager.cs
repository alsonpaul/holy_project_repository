﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour 
{
    public int level_Score_Req_1;
    public int level_Score_Req_2;
    public int level_Score_Req_3;
    
    public int currentScore;

    private bool levelFading = false;
    public float fadeSpeed = 1.5f;
    public Image FadeScreen;

    void Update()
    {
        if (levelFading == true)
        {
            FadeScreen.color = Color.Lerp(FadeScreen.color, Color.black, fadeSpeed * Time.deltaTime);
        }
    }

    public void WinLevel()
    {
        levelFading = true;
        Debug.Log("Congratualation!");
    }
}
