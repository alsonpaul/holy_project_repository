﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour 
{
    public List<AudioClip> BGMusic = new List<AudioClip>();
    public List<AudioClip> SFX = new List<AudioClip>();

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
