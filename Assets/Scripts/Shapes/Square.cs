﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShapeEvent : GameEvent
{
    public Vector3 position;

    public ShapeEvent(Vector3 pos)
    {
        position = pos;
    }
}

public class ScaleEvent : GameEvent
{
    public float scaleRatio;

    public ScaleEvent(float ratio)
    {
        scaleRatio = ratio;
    }
}
public class Square : MonoBehaviour 
{
    void OnEnable()
    {
        this.AddEventListenerGlobal<ShapeEvent>(OnTouchDown);
        this.AddEventListenerGlobal<ScaleEvent>(OnTouchHold);
    }

    void OnTouchDown(ShapeEvent e)
    {
        Debug.Log("Hey");
        this.transform.position = new Vector3(e.position.x, e.position.y, this.transform.position.z);
    }

    void OnTouchHold(ScaleEvent e)
    {
        this.transform.localScale += new Vector3(e.scaleRatio * Time.deltaTime, e.scaleRatio * Time.deltaTime, e.scaleRatio  * Time.deltaTime);
    }
}
