﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections.ObjectModel;

public class BallScript : MonoBehaviour
{
    public ScoreManager Manager;
	public List<GameObject> Star;
	public GameObject End;

	void OnCollisionEnter(Collision other)
	{
        for (int index = 0; index < Star.Count; index++)
        {
            if (other.gameObject == Star[index])
            {
                Manager.currentScore += 1000;
                Debug.Log(Manager.currentScore);
                Destroy(other.gameObject);
            }
        }
        if (other.gameObject == End)
        {
            Manager.WinLevel();
        }
	}
}
