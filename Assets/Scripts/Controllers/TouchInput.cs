﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchInput : MonoBehaviour 
{
	public LayerMask touchInputMask;

    public List<GameObject> shapeList = new List<GameObject>();
    private GameObject[] touchOld;
    private RaycastHit hit;
    void Update()
    {
        // PC INPUT
#if UNITY_EDITOR
        if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
        {
            touchOld = new GameObject[shapeList.Count];
            shapeList.CopyTo(touchOld);
            shapeList.Clear();

            Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, touchInputMask))
            {
                GameObject shape = hit.transform.gameObject;
                shapeList.Add(shape);
                if (Input.GetMouseButton(0))
                {
                    // event system here
                    this.RaiseEventGlobal<ShapeEvent>(new ShapeEvent(hit.point));
                   // shape.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);

                }
            }

            // if the objects are not touched
            foreach (GameObject shape in touchOld)
            {
                if (!shapeList.Contains(shape))
                {
                    // event
                }
            }
        }
#endif
        // MOBILE INPUT
        if (Input.touchCount > 0)
        {
            touchOld = new GameObject[shapeList.Count];
            shapeList.CopyTo(touchOld);
            shapeList.Clear();
            foreach (Touch touch in Input.touches)
            {
                Ray ray = GetComponent<Camera>().ScreenPointToRay(touch.position);

                if (Physics.Raycast(ray, out hit, touchInputMask))
                {
                    GameObject shape = hit.transform.gameObject;
                    shapeList.Add(shape);        
                    if (touch.phase == TouchPhase.Moved)
                    {
                        // event system here
                        //shape.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
                        this.RaiseEventGlobal<ShapeEvent>(new ShapeEvent(hit.point));

                    }
                    if (touch.phase == TouchPhase.Stationary)
                    {
                       // this.RaiseEventGlobal<ScaleEvent>(new ScaleEvent());
                    }
                    if (Input.touchCount == 2)
                    {
                        Touch touch_0 = Input.GetTouch(0);
                        Touch touch_1 = Input.GetTouch(1);

                        Vector2 touch_0_Position = touch_0.position - touch_0.deltaPosition;
                        Vector2 touch_1_Position = touch_1.position - touch_1.deltaPosition;

                        float prevTouchMag = (touch_0_Position - touch_1_Position).magnitude;
                        float deltaTouchMag = (touch_0.position - touch_1.position).magnitude;

                        float deltaMagnitudeDiff = prevTouchMag - deltaTouchMag;

                        this.RaiseEventGlobal<ScaleEvent>(new ScaleEvent(-deltaMagnitudeDiff));
                    }
                }
            }

            foreach (GameObject shape in touchOld)
            {
                if (!shapeList.Contains(shape))
                {
                    // event
                }
            }
        }
    }
}